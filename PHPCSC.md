# PHP Code Style Conventions (PHPCSC)

Соглашения по оформлению PHP кода.  
[На главную](./README.md)

## Содержание

0. [Общие правила](#Общие-правила)
  
## 1. **Общие правила**

1.1. Необходимо использовать стандарты [PSR-1](https://www.php-fig.org/psr/psr-1) (на [русском](https://github.com/samdark/fig-standards-ru/blob/master/accepted/ru/PSR-1-basic-coding-standard.md)) и [PSR-2](https://www.php-fig.org/psr/psr-2) (на [русском](https://github.com/samdark/fig-standards-ru/blob/master/accepted/ru/PSR-2-coding-style-guide.md)) 